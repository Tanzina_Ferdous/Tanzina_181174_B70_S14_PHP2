<?php
//php support 8 DataTypes

//compound types (2) :
//associative array

    $age = [

        "Rahim"=> 23,  //rahim is a key
        "Karim"=> 30,
        "Jorina"=> 23,
        "Moynar Ma"=> 33
    ];

    echo $age["Moynar Ma"];

   $mixd = [      // mixed array
        34,
        66,
    "Rahim"=> 23,  //rahim is a key
    "Karim"=> 30,
    "Jorina"=> 23,
       54,
    "Moynar Ma"=> 33,
       88
    ];
    echo $mixd[3];







die;


//compound types (2) :

class MyClass
    {
        public $thisIsAProperty;

        /**
         * @return mixed
         */
        public function getThisIsAProperty()
        {
            return $this->thisIsAProperty;
        }

        /**
         * @param mixed $thisIsAProperty
         */
        public function setThisIsAProperty($thisIsAProperty)
        {
            $this->thisIsAProperty = $thisIsAProperty;
        }
    }
    $myObject1 = new MyClass();
    $myObject2 = new MyClass();

    $myObject1->setThisIsAProperty(225);

    echo  $myObject2->getThisIsAProperty();


   // $myArray = [23, "Hello ",23,4,2,3 ];  //Indexed Array

    $myArray = [23, "Hello ",[23, "Hello ",23,4,2,3 ], 23 ,4.5 ,2,3 ];  //Indexed Array

  //  $myArray = array( 23,"Hello",23,4,2,3);

  //  $myArray = array( 23,"Hello", array( 23,"Hello",23,4,2,3), 23,4,2,3);

    echo $myArray[2][1];
    $myArray[3]= "world";
    echo $myArray[3] ;


//scalar types 4

    $thisIsVariable = 123; //Integer

    $thisIsVariable = 12.3; //float

    $thisIsVariable = true; //Boolean

    $x = 34;

    $thisIsVariable= "This $x is a \"double\" 'quoted' string<br>";  //string

    echo $thisIsVariable;

    $thisIsVariable='This $x is a \'single\' "quoted" string'; //string

    echo $thisIsVariable;

    //Heredoc. Similar to double quote
    $thisIsVariable = <<<BASIS
    
    কিছুদিন আগেই পথ দেখিয়েছেন এনামুল হক। পাকিস্তান সুপার লিগের ফাইনাল খেলতে গিয়েছিলেন এই টপ অর্ডার ব্যাটসম্যান। 
    সবকিছু ঠিক থাকলে গত ৯ বছরে দ্বিতীয় বাংলাদেশি ব্যাটসম্যান হিসেবে পাকিস্তানে খেলতে দেখা যাবে তামিম ইকবালকে। 
    আগামী মাসে পাকিস্তানের বিপক্ষে বিশ্ব একাদশের পক্ষে ওপেন করতে নামার কথা বাংলাদেশের সেরা ব্যাটসম্যানের।
    
    $x

BASIS;

    echo $thisIsVariable;

    //Nowdoc.This is similar to single quote
    $thisIsVariable = <<<'BASIS'
    
    কিছুদিন আগেই পথ দেখিয়েছেন এনামুল হক। পাকিস্তান সুপার লিগের ফাইনাল খেলতে গিয়েছিলেন এই টপ অর্ডার ব্যাটসম্যান। 
    সবকিছু ঠিক থাকলে গত ৯ বছরে দ্বিতীয় বাংলাদেশি ব্যাটসম্যান হিসেবে পাকিস্তানে খেলতে দেখা যাবে তামিম ইকবালকে। 
    আগামী মাসে পাকিস্তানের বিপক্ষে বিশ্ব একাদশের পক্ষে ওপেন করতে নামার কথা বাংলাদেশের সেরা ব্যাটসম্যানের।
    
    $x
    
BASIS;
    echo $thisIsVariable;

$thisIsVariable = <<<'BASIS'
    <!doctype html>
    <html lang="en">
    <head>
    <meta charset="UTF-8">
    <title>Document</title>
    
    
    
BASIS;



